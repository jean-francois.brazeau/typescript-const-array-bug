#!/bin/bash
# Exit on error
set -e

# Check arguments
if [ "$1" = "" ]
then
    echo "Please specify typescript version"
    exit -1
fi

# Install typescript
echo "Installing typescrip $1"
npm install --save-dev typescript@$1

# Compile
npx tsc --version
npx tsc
npx node ./dist/index.js

# We shouldn't reach that point, a compilation failure should have been raised
echo "ERROR: No compilation error raised !"
exit -1