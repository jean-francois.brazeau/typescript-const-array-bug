// Define a read-only array of strings (fruit names)
const FRUITS = [
  "apple",
  "banana",
  "blackberry",
  "cherry",
  "orange",
  "raspberry",
] as const;

// Define a type according to the string array
type Fruits = (typeof FRUITS)[number];

/**
 * Should be defined as 'readonly Fruit[]' (missing readonly keyword).
 * tsc fails with 5.1.3 or 5.2.2
 * tsc doesn't raise a compilation error with 5.3.3
 */
const RED_FRUITS: Fruits[] = ["cherry", "raspberry"] as const;

// As RED_FRUITS is not readonly, we can even push values into the array (that was expected
// to be readonly)
RED_FRUITS.push("apple");

// Print the array containing the apple !
console.log(RED_FRUITS);
